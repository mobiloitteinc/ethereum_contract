pragma solidity ^0.4.11;

import './MintableToken.sol';

library Whitelist {
  
  struct List {
    mapping(address => bool) registry;
  }
  
  function add(List storage list, address _addr)
    internal
  {
    list.registry[_addr] = true;
  }

  function remove(List storage list, address _addr)
    internal
  {
    list.registry[_addr] = false;
  }

  function check(List storage list, address _addr)
    view
    internal
    returns (bool)
  {
    return list.registry[_addr];
  }
}

contract Whitelisted  {

  Whitelist.List private _list;
  
  modifier onlyWhitelisted() {
    require(Whitelist.check(_list, msg.sender) == true);
    _;
  }

  event AddressAdded(address _addr);
  event AddressRemoved(address _addr);
  
  function WhitelistedAddress()
  public
  {
    Whitelist.add(_list, msg.sender);
  }

  function WhitelistAddressenable(address _addr)
    public
  {
    Whitelist.add(_list, _addr);
    emit AddressAdded(_addr);
  }

  function WhitelistAddressdisable(address _addr)
    public
  {
    Whitelist.remove(_list, _addr);
   emit AddressRemoved(_addr);
  }
  
  function WhitelistAddressisListed(address _addr)
  public
  view
  returns (bool)
  {
      return Whitelist.check(_list, _addr);
  }
}

contract WhitelistToken is Whitelisted {

  function onlyWhitelistedCanDo()
    onlyWhitelisted
    view
    external
  {    
  }

}

