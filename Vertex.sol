pragma solidity ^0.4.11;

import './MintableToken.sol';
import './Whitelisted.sol';
import "github.com/oraclize/ethereum-api/oraclizeAPI.sol";

//For production, change all days to days
//Change and check days and discounts
contract Vertex_Token is Ownable, usingOraclize, Whitelisted {
    using SafeMath for uint256;

    // The token being sold
    MintableToken public token;

    // start and end timestamps where investments are allowed (both inclusive)
    // uint256 public PrivateSaleStartTime;
    // uint256 public PrivateSaleEndTime;
    uint256 public ICOStartTime = 1535356800;
    uint256 public ICOEndTime = 1544860800;

    uint256 public hardCap = 180000000;

    // address where funds are collected
    address public wallet;

    // how many token units a buyer gets per wei
    uint256 public rate;
    uint256 public weiRaised;

    /**
    * event for token purchase logging
    * @param purchaser who paid for the tokens
    * @param beneficiary who got the tokens
    * @param value weis paid for purchase
    * @param amount amount of tokens purchased
    */

    event TokenPurchase(address indexed purchaser, address indexed beneficiary, uint256 value, uint256 amount);
    event newOraclizeQuery(string description);

    function Vertex_Token(uint256 _rate, address _wallet, uint256 _unfreeze_date)  public {
        require(_rate > 0);
        require(_wallet != address(0));

        token = createTokenContract();

        rate = _rate;
        wallet = _wallet;
        
        token.changeFreezeDate(_unfreeze_date);
    }
   
   
    // function startICO() onlyOwner public {
    //     require(ICOStartTime == 0);
    //     ICOStartTime = now;
    //     ICOEndTime = ICOStartTime + 112 days;
    // }
    // function stopICO() onlyOwner public {
    //     require(ICOEndTime > now);
    //     ICOEndTime = now;
    // }
    
    function changeTokenFreezeDate(uint256 _new_date) onlyOwner public {
        token.changeFreezeDate(_new_date);
    }
    
    function unfreezeTokens() onlyOwner public {
        token.changeFreezeDate(now);
    }

    // creates the token to be sold.
    // override this method to have crowdsale of a specific mintable token.
    function createTokenContract() internal returns (MintableToken) {
        return new MintableToken();
    }

    // fallback function can be used to buy tokens
    function () payable public {
        buyTokens(msg.sender);
    }



    //return token price in cents
    function getUSDPrice() public constant returns (uint256 cents_by_token) {
        uint256 total_tokens = SafeMath.div(totalTokenSupply(), token.decimals());

        if (total_tokens > 165000000)
            return 31;
        else if (total_tokens > 150000000)
            return 30;
        else if (total_tokens > 135000000)
            return 29;
        else if (total_tokens > 120000000)
            return 28;
        else if (total_tokens > 105000000)
            return 27;
        else if (total_tokens > 90000000)
            return 26;
        else if (total_tokens > 75000000)
            return 25;
        else if (total_tokens > 60000000)
            return 24;
        else if (total_tokens > 45000000)
            return 23;
        else if (total_tokens > 30000000)
            return 22;
        else if (total_tokens > 15000000)
            return 18;
        else
            return 15;
    }
    // function calcBonus(uint256 tokens, uint256 ethers) public constant returns (uint256 tokens_with_bonus) {
    //     return tokens;
    // }
    // string 123.45 to 12345 converter
    function stringFloatToUnsigned(string _s) payable public returns (string) {
        bytes memory _new_s = new bytes(bytes(_s).length - 1);
        uint k = 0;

        for (uint i = 0; i < bytes(_s).length; i++) {
            if (bytes(_s)[i] == '.') { break; } // 1

            _new_s[k] = bytes(_s)[i];
            k++;
        }

        return string(_new_s);
    }
    // callback for oraclize 
    function __callback(bytes32 myid, string result) public {
        if (msg.sender != oraclize_cbAddress()) revert();
        string memory converted = stringFloatToUnsigned(result);
        rate = parseInt(converted);
        rate = SafeMath.div(1000000000000000000, rate); // price for 1 USD in WEI 
    }
    // price updater 
    function updatePrice() payable public {
        oraclize_setProof(proofType_NONE);
        if (oraclize_getPrice("URL") > address(this).balance) {
         emit newOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
        } else {
         emit   newOraclizeQuery("Oraclize query was sent, standing by for the answer..");
            oraclize_query("URL", "json(https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD).USD");
        }
    }
    // low level token purchase function
    function buyTokens(address beneficiary) public payable {
        require(beneficiary != address(0));
        require(WhitelistAddressisListed(beneficiary));
        require(validPurchase());
        require(msg.value >= SafeMath.mul(rate, 50));  // minimum contrib amount 50 USD
        
        updatePrice();

        uint256 _convert_rate = SafeMath.div(SafeMath.mul(rate, getUSDPrice()), 100);

        // calculate token amount to be created
        uint256 weiAmount = SafeMath.mul(msg.value, 10**uint256(token.decimals()));
        uint256 tokens = SafeMath.div(weiAmount, _convert_rate);
        require(tokens > 0);
        
        //do not need bonus of contrib amount calc
        // tokens = calcBonus(tokens, msg.value.div(10**uint256(token.decimals())));

        // update state
        weiRaised = SafeMath.add(weiRaised, msg.value);

        token.mint(beneficiary, tokens);
        emit TokenPurchase(msg.sender, beneficiary, msg.value, tokens);

        forwardFunds();
    }


    //to send tokens for bitcoin bakers and bounty
    function sendTokens(address _to, uint256 _amount) onlyOwner public {
        token.mint(_to, _amount);
    }
    //change owner for child contract
    function transferTokenOwnership(address _newOwner) onlyOwner public {
        token.transferOwnership(_newOwner);
    }

    // send ether to the fund collection wallet
    // override to create custom fund forwarding mechanisms
    function forwardFunds() internal {
        wallet.transfer(address(this).balance);
    }

    // @return true if the transaction can buy tokens
    function validPurchase() internal constant returns (bool) {
        bool hardCapOk = token.totalSupply() < SafeMath.mul(hardCap, 10**uint256(token.decimals()));
       // bool withinPrivateSalePeriod = now >= PrivateSaleStartTime && now <= PrivateSaleEndTime;
        bool withinICOPeriod = now >= ICOStartTime && now <= ICOEndTime;
        bool nonZeroPurchase = msg.value != 0;
        
        // private-sale hardcap
        uint256 total_tokens = SafeMath.div(totalTokenSupply(), token.decimals());
        // if (withinPrivateSalePeriod && total_tokens >= 30000000)
        // {
        //     stopPrivateSale();
        //     return false;
        // }
        
        // return hardCapOk && (withinICOPeriod || withinPrivateSalePeriod) && nonZeroPurchase;
         return hardCapOk && withinICOPeriod && nonZeroPurchase;
    }
    
    // total supply of tokens
    function totalTokenSupply() public view returns (uint256) {
        return token.totalSupply();
    }
}