pragma solidity ^0.4.11;


import './Ownable.sol';
import './StandardToken.sol';

contract TokenFreeze is Ownable, StandardToken {
  uint256 public unfreeze_date;
  
  event FreezeDateChanged(string message, uint256 date);

  function TokenFreeze() public {
    unfreeze_date = now;
  }

  modifier freezed() {
    require(unfreeze_date < now);
    _;
  }

  function changeFreezeDate(uint256 datetime) onlyOwner public {
    require(datetime != 0);
    unfreeze_date = datetime;
  emit  FreezeDateChanged("Unfreeze Date: ", datetime);
  }
  
  function transferFrom(address _from, address _to, uint256 _value) freezed public returns (bool) {
    super.transferFrom(_from, _to, _value);
  }

  function transfer(address _to, uint256 _value) freezed public returns (bool) {
    super.transfer(_to, _value);
  }

}